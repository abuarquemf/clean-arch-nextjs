import CustomerRepository from "../domain/repository/customer_repository";
import IDService from "./services/id_service";
import CreateAccountOutput from "./create_account/create_account_output";
import CreateAccountInput from "./create_account/create_account_input";
import CreateaAccountUseCase from "./create_account/create_account_use_case";
import PasswordService from "./services/password_service";
import { Injectable, Inject } from "@nestjs/common";
import LoginUseCase from "./login/login_use_case";
import { LoginInput } from "./login/login_input";
import { LoginOutput } from "./login/login_output";

@Injectable()
export default class UseCases {

    private createAccountUseCase: CreateaAccountUseCase;
    private loginUseCase: LoginUseCase;

    constructor(@Inject('CustomerRepository') customerRepository: CustomerRepository,
        @Inject('IDService') idService: IDService,
        @Inject('PasswordService') passwordService: PasswordService) {
        this.createAccountUseCase = new CreateaAccountUseCase(customerRepository, idService, passwordService);
        this.loginUseCase = new LoginUseCase(customerRepository, passwordService);
    }

    create(input: CreateAccountInput): CreateAccountOutput {
        return this.createAccountUseCase.create(input);
    }

    login(input: LoginInput): LoginOutput {
        return this.loginUseCase.login(input);
    }
}
