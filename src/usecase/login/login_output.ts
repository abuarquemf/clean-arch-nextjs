export class LoginOutput {
    ID: string;
    name: string;
    email: string;

    constructor(ID: string, name: string, email: string) {
        this.name = name;
        this.ID = ID;
        this.email = email;
    }
}