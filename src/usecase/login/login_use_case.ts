import CustomerRepository from "src/domain/repository/customer_repository";
import PasswordService from "../services/password_service";
import { LoginInput } from "./login_input";
import { LoginOutput } from "./login_output";
import UseCaseException from "../exceptions/use_case_exception";

export default class LoginUseCase {
    private customerRepository: CustomerRepository;
    private passwordService: PasswordService;

    constructor(customerRepository: CustomerRepository, passwordService: PasswordService) {
        this.customerRepository = customerRepository;
        this.passwordService = passwordService;
    }

    login(input: LoginInput): LoginOutput {
        const user = this.customerRepository.findByEmail(input.email);
        if (user == null) {
            throw new UseCaseException(`not found account with email ${input.email}`, 404);
        }
        if (!this.passwordService.isValid(input.password, user.password)) {
            throw new UseCaseException(`invalid credentials`, 409);
        }
        return new LoginOutput(user.ID, user.name, user.email);
    }
}