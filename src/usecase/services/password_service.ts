export default interface PasswordService {

    encrypt(password: string): string

    isValid(passwordToCheck: string, passwordFromDatasource: string): boolean
}
