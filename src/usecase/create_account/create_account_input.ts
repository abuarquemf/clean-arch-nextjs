export default class CreateAccountInput {
    name: string;
    password: string;
    email: string;

    constructor(name: string, email: string, password: string) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    static fromRequestBody(responseBody: any): CreateAccountInput {
        return new CreateAccountInput(responseBody.name, responseBody.email, responseBody.password);
    }
}
