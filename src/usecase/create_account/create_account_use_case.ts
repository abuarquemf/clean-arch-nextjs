import CustomerRepository from "../../domain/repository/customer_repository";
import Customer from "../../domain/model/customer";
import CreateAccountInput from "./create_account_input";
import CreateAccountOutput from "./create_account_output";
import IDService from "../services/id_service";
import PasswordService from "../services/password_service";
import UseCaseException from "../exceptions/use_case_exception";

export default class CreateaAccountUseCase {
    private customerRepository: CustomerRepository;
    private idService: IDService;
    private passwordService: PasswordService;

    constructor(customerRepository: CustomerRepository, idService: IDService, passwordService: PasswordService) {
        this.customerRepository = customerRepository;
        this.idService = idService;
        this.passwordService = passwordService;
    }

    create(input: CreateAccountInput): CreateAccountOutput {
        if (this.customerRepository.findByEmail(input.email) !== null) {
            throw new UseCaseException(`email ${input.email} already in use.`, 409);
        }
        const id = this.idService.get();
        const password = this.passwordService.encrypt(input.password);
        let user = new Customer(id, input.name, input.email, password);
        try {
            this.customerRepository.save(user);
        } catch (e) {
            throw new UseCaseException(`failed to save new customer. Erro: ${e}`, 409);
        }
        return new CreateAccountOutput(user.ID, user.name, user.email);
    }
}
