export default class Customer {
    ID: string;
    name: string;
    email: string;
    password: string;

    constructor(ID: string, name: string, email: string, password: string) {
        this.ID = ID;
        this.name = name;
        this.email = email;
        this.password = password;
    }
}
