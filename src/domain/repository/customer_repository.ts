import Customer from "../model/customer";

export default interface CustomerRepository {
    findByID(id: string): Customer | null;

    findByEmail(email: string): Customer | null;

    save(customer: Customer): Customer
}
