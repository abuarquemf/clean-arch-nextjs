import { Module } from '@nestjs/common';
import UUIDService from './adapter/id/id_service';
import BycryptPasswordService from './adapter/password/bycrypt_password';
import InMemoryRepository from './adapter/persistence/in_memory_repository';
import AppController from './adapter/controller/app_controller';
import UseCases from './usecase/use_case';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [UseCases, { provide: "IDService", useClass: UUIDService }, { provide: "PasswordService", useClass: BycryptPasswordService }, { provide: "CustomerRepository", useClass: InMemoryRepository }],
})
export class AppModule { }
