import UseCases from "../../usecase/use_case";
import CreateAccountInput from "../../usecase/create_account/create_account_input";
import CreateAccountOutput from "../../usecase/create_account/create_account_output";
import { Controller, Post, Body, HttpException, HttpStatus, HttpCode } from '@nestjs/common';
import UseCaseException from "src/usecase/exceptions/use_case_exception";
import { LoginInput } from "src/usecase/login/login_input";
import { LoginOutput } from "src/usecase/login/login_output";

@Controller("v1/customers")
export default class AppController {
    private useCases: UseCases;

    constructor(useCases: UseCases) {
        this.useCases = useCases;
    }

    @Post()
    create(@Body() createAccountInput: CreateAccountInput): CreateAccountOutput {
        try {
            const useCaseResponse: CreateAccountOutput = this.useCases.create(createAccountInput);
            return useCaseResponse;
        } catch (e) {
            if (e instanceof UseCaseException) {
                throw new HttpException({ message: e.message, }, e.code);
            }
            throw new HttpException({ message: "Unexpected error", }, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Post("login")
    @HttpCode(200)
    login(@Body() loginInput: LoginInput): LoginOutput {
        try {
            const useCaseResponse = this.useCases.login(loginInput);
            return useCaseResponse;
        } catch (e) {
            if (e instanceof UseCaseException) {
                throw new HttpException({ message: e.message, }, e.code);
            }
            throw new HttpException({ message: "Unexpected error", }, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
