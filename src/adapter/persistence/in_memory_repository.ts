import CustomerRepository from "../../domain/repository/customer_repository";
import Customer from "../../domain/model/customer";
import { Injectable } from "@nestjs/common";

@Injectable()
export default class InMemoryRepository implements CustomerRepository {
    private db: Customer[];

    constructor() {
        this.db = [];
    }

    findByID(id: string): Customer | null {
        for (let customer of this.db) {
            if (customer.ID == id) {
                return customer;
            }
        }
        return null;
    }

    findByEmail(email: string): Customer | null {
        for (let customer of this.db) {
            if (customer.email == email) {
                return customer;
            }
        }
        return null;
    }

    save(customer: Customer): Customer {
        this.db.push(customer);
        return customer;
    }
}
