import IDService from "../../usecase/services/id_service";
import { v4 as uuid } from "uuid";
import { Injectable } from "@nestjs/common";

@Injectable()
export default class UUIDService implements IDService {

    get(): string {
        return uuid();
    }
}