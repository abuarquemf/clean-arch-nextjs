import PasswordService from "../../usecase/services/password_service";
import * as bcrypt from 'bcryptjs';
import { Injectable } from "@nestjs/common";

@Injectable()
export default class BycryptPasswordService implements PasswordService {

    encrypt(password: string): string {
        let salt = bcrypt.genSaltSync(10);
        return bcrypt.hashSync(password, salt);
    }

    isValid(passwordToCheck: string, passwordFromDatasource: string): boolean {
        return bcrypt.compareSync(passwordToCheck, passwordFromDatasource)
    }
}
